const jwt = require('jsonwebtoken')
const keys = require('../../resources/keys')
const AdminModel = require('../models/admin/admin').model

exports.AuthenticateUser = (req, res, next) => {
    try {
        const token = req.header('Authorization').replace('Bearer ', '')

        const userId = jwt.verify(token, keys.tokenJWTKey)
        req.auth = userId

        req.UserModel.findOne({
            where: {
                id: userId
            },
            attributes: {
                exclude: [
                    'password',
                    'createdAt',
                    'updatedAt',
                ]
            }

        })
            .then((data) => {
                if (data == null) throw new Error("User not found")
                req.authUser = data
                next()
            })
            .catch((e) => {
                console.log(e)
                res.send(e)
            })



    } catch (err) {
        return res.status(401).json({
            status: false,
            message: 'Invalid Token',
        })
    }
}


exports.AuthenticateAdmin = (req, res, next) => {
    try {
        const token = req.header('Authorization').replace('Bearer ', '')

        const userId = jwt.verify(token, keys.tokenJWTKey)
        req.auth = userId

        AdminModel.findOne({
            where: {
                id: userId
            },
            attributes: {
                exclude: [
                    'password',
                    'createdAt',
                    'updatedAt',
                ]
            }

        })
            .then((data) => {
                if (data == null) throw new Error("User not found")
                req.authAdmin = data
                next()
            })
            .catch((e) => {
                console.log(e)
                res.send(e)
            })



    } catch (err) {
        return res.status(401).json({
            status: false,
            message: 'Invalid Token',
        })
    }
}

