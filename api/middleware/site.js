const SubDomain = require('../models/subdomains/subdomains')
const AdminSubDomain = require('../models/subdomains/adminSubdomain')
const connection = require('../../services/sequelize.service').connection()
const AdminNudgeSchema = require('../models/admin/nudges')
const UserSchema = require('../models/users')
const MemoSchema = require('../models/user/memos')
const NudgeSchema = require('../models/user/nudges')
const QueriesSchema = require('../models/user/queries')
const CustomNudgeSchema = require('../models/user/custom_nudges')
const Sequelize = require('sequelize')

const { createModels, createAdminModel } = require('../models/associations/localModel')

let models = {}
let adminModels = {}

exports.returnModels = () => models
exports.addToModel = (key, value) => {
    models[key] = value
}

// User

exports.AddSubDomain = async (req, res, next) => {
    const _b = req.body

    SubDomain.create({
        subdomain: _b.subdomain,
    })
        .then(async (data) => {

            await createModels(data.id, connection)
                .then(async (e) => {
                    models[data.id] = e
                    console.log(models)
                    await connection.sync()
                    req.SubDomain = data
                    next()
                })

        })
        .catch(error => {
            console.log(error)
            res.send(error)
        })
}

exports.UserModel = async (req, res, next) => {
    const _b = req.body
    console.log(models)
    try {
        if (models[_b.subdomain] === undefined || models[_b.subdomain].User === undefined) {
            // console.log("\n\n\n\n\n\n\n\n\n\n\nIn in")
            // const model = connection.define('users' + "_" + _b.subdomain, UserSchema.options, UserSchema.hooks)

            // const modelQueries = connection.define('queries' + "_" + _b.subdomain, QueriesSchema.options)

            // model.hasOne(modelQueries, {
            //     foreignKey: "user" + _b.subdomain + "Id"
            // })
            // modelQueries.belongsTo(modelQueries, {
            //     foreignKey: "user" + _b.subdomain + "Id"
            // })
            const { createModels } = require('../models/associations/localModel')

            const allModels = await createModels(_b.subdomain, require('../../services/sequelize.service').connection())
            const model = allModels.User

            let newVal = models[_b.subdomain] || {}
            newVal.User = model
            model[_b.subdomain] = newVal

            req.UserModel = model
        } else {
            req.UserModel = models[_b.subdomain].User
        }

        next()
    } catch (error) {
        console.log(error)
        res.send(error)
    }
}

exports.MemoModel = async (req, res, next) => {
    const _b = req.body

    try {
        if (models[_b.subdomain] === undefined || models[_b.subdomain].Memo === undefined) {
            // const model = connection.define('memos' + "_" + _b.subdomain, MemoSchema.options)

            const { createModels } = require('../models/associations/localModel')

            const allModels = await createModels(_b.subdomain, require('../../services/sequelize.service').connection())
            const model = allModels.Memo

            let newVal = models[_b.subdomain] || {}
            newVal.Memo = model
            model[_b.subdomain] = newVal

            req.MemoModel = model
        } else {
            req.MemoModel = models[_b.subdomain].Memo
        }

        next()
    } catch (error) {
        console.log(error)
        res.send(error)
    }


}

exports.NudgeModel = async (req, res, next) => {
    const _b = req.body

    try {
        if (models[_b.subdomain] === undefined || models[_b.subdomain].Nudge === undefined) {
            // const model = connection.define('nudges' + "_" + _b.subdomain, NudgeSchema.options)
            const { createModels } = require('../models/associations/localModel')

            const allModels = await createModels(_b.subdomain, require('../../services/sequelize.service').connection())
            const model = allModels.Nudge

            let newVal = models[_b.subdomain] || {}
            newVal.Nudge = model
            model[_b.subdomain] = newVal

            req.NudgeModel = model
        } else {
            req.NudgeModel = models[_b.subdomain].Nudge
        }

        next()
    } catch (error) {
        console.log(error)
        res.send(error)
    }


}

exports.CustomNudgeModel = async (req, res, next) => {
    const _b = req.body

    try {
        if (models[_b.subdomain] === undefined || models[_b.subdomain].CustomNudge === undefined) {
            const { createModels } = require('../models/associations/localModel')

            const allModels = await createModels(_b.subdomain, require('../../services/sequelize.service').connection())
            const model = allModels.CustomNudge

            let newVal = models[_b.subdomain] || {}
            newVal.CustomNudge = model
            model[_b.subdomain] = newVal

            req.CustomNudgeModel = model
        } else {
            req.CustomNudgeModel = models[_b.subdomain].CustomNudge
        }

        next()
    } catch (error) {
        console.log(error)
        res.send(error)
    }


}

exports.QueryModel = async (req, res, next) => {
    const _b = req.body
    try {
        if (models[_b.subdomain] === undefined || models[_b.subdomain].Query === undefined) {
            const { createModels } = require('../models/associations/localModel')

            const allModels = await createModels(_b.subdomain, require('../../services/sequelize.service').connection())
            const model = allModels.Query
            let newVal = models[_b.subdomain] || {}
            newVal.Query = model
            model[_b.subdomain] = newVal

            req.QueryModel = model
        } else {
            req.QueryModel = models[_b.subdomain].Query
        }

        next()
    } catch (error) {
        console.log(error)
        res.send(error)
    }

}

exports.PostModel = async (req, res, next) => {
    const _b = req.body
    try {
        if (models[_b.subdomain] === undefined || models[_b.subdomain].Query === undefined) {
            const { createModels } = require('../models/associations/localModel')

            const allModels = await createModels(_b.subdomain, require('../../services/sequelize.service').connection())
            const model = allModels.Post
            let newVal = models[_b.subdomain] || {}
            newVal.Post = model
            model[_b.subdomain] = newVal

            req.PostModel = model
        } else {
            req.PostModel = models[_b.subdomain].Post
        }

        next()
    } catch (error) {
        console.log(error)
        res.send(error)
    }

}

exports.MediaModel = async (req, res, next) => {
    const _b = req.body
    try {
        if (models[_b.subdomain] === undefined || models[_b.subdomain].Query === undefined) {
            const { createModels } = require('../models/associations/localModel')

            const allModels = await createModels(_b.subdomain, require('../../services/sequelize.service').connection())
            const model = allModels.Media
            let newVal = models[_b.subdomain] || {}
            newVal.Media = model
            model[_b.subdomain] = newVal

            req.MediaModel = model
        } else {
            req.MediaModel = models[_b.subdomain].Media
        }

        next()
    } catch (error) {
        console.log(error)
        res.send(error)
    }

}

// Association Models

exports.UserNudgeModel = async (req, res, next) => {
    const _b = req.body

    try {
        if (models[_b.subdomain] === undefined || models[_b.subdomain].UserNudge === undefined) {
            // const model = connection.define('nudges' + "_" + _b.subdomain, NudgeSchema.options)
            const { createModels } = require('../models/associations/localModel')

            const allModels = await createModels(_b.subdomain, require('../../services/sequelize.service').connection())
            const model = allModels.UserNudge

            let newVal = models[_b.subdomain] || {}
            newVal.Nudge = model
            model[_b.subdomain] = newVal

            req.UserNudgeModel = model
        } else {
            req.UserNudgeModel = models[_b.subdomain].UserNudge
        }

        next()
    } catch (error) {
        console.log(error)
        res.send(error)
    }


}



// Admin

exports.AddAdminSubDomain = async (req, res, next) => {
    const _b = req.body

    AdminSubDomain.create({
        subdomain: _b.subdomain,
    })
        .then(async (data) => {

            await createAdminModel(data.id, connection)
                .then(async (e) => {

                    adminModels[data.id] = e
                    await connection.sync()
                    req.AdminSubDomain = data
                    next()

                })

        })
        .catch(error => {
            console.log(error)
            res.send(error)
        })
}

exports.AdminNudgeModel = async (req, res, next) => {
    const _b = req.body

    req.AdminNudgeModel = AdminNudgeSchema.model
    next()
    // try {
    //     if (adminModels[_b.id] === undefined || adminModels[_b.id].Nudge === undefined) {
    //         const model = connection.define('admin_nudges' + "_" + _b.id, NudgeSchema.options)
    //         let newVal = adminModels[_b.id] || {}
    //         newVal.Nudge = model
    //         adminModels[_b.id] = newVal

    //         req.AdminNudgeModel = model
    //     } else {
    //         req.AdminNudgeModel = models[_b.id].Nudge
    //     }

    //     next()
    // } catch (error) {
    //     console.log(error)
    //     res.send(error)
    // }


}