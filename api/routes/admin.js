const AdminController = require('../controllers/admin/admin.controller')
const express = require('express')
const checkAuth = require('../middleware/check_auth')
const QuestionsController = require('../controllers/admin/assessment.controller')

const router = express.Router()


router.post('/add', AdminController.addAdmin)
router.post('/login', AdminController.loginAdmin)
router.post('/addSubdomain', AdminController.addSubDomain)
router.post('/listAllSubdomains', AdminController.listAllSubdomains)
router.post('/listAllAdmins', AdminController.listAllAdmins)
router.post('/addAdminNudge', AdminController.AddAdminNudge)
router.post('/AddAdminNudgeBulk', AdminController.AddAdminNudgeBulk)

//Assessment
router.post('/seedQuestions', QuestionsController.seedAssessment)
router.post('/fetchQuestions', QuestionsController.fetchQuestions)



// Test
router.post('/testCheck', checkAuth.AuthenticateAdmin, (req, res) => {
    console.log(req.auth)
    console.log(req.authAdmin)

    res.send(req.authAdmin)
})

module.exports = router
