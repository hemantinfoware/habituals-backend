const express = require('express')
const multer = require('../../services/multer.service')

const checkAuth = require('../middleware/check_auth')


const UserController = require('../controllers/users/user.controller')
const NudgeController = require('../controllers/users/nudges.controller')
const QuestionsController = require('../controllers/users/questions.controller')
const MemoController = require('../controllers/users/memos.controller')
const MediaController = require('../controllers/users/media.controller')
const PostController = require('../controllers/users/post.controller')

const testController = require('../middleware/site')

const router = express.Router()

// API to register user into the database

router.post('/register', testController.UserModel, UserController.register)
router.post('/login', testController.UserModel, UserController.login)
router.post('/startSession', testController.QueryModel,testController.NudgeModel, testController.UserNudgeModel, UserController.startSession)
router.post('/questions', testController.QueryModel, QuestionsController.seedQuestions)
router.post('/questionsUpdate', testController.QueryModel, QuestionsController.updateQuestions)
router.post('/profile', testController.QueryModel, testController.UserModel, UserController.profile)
// router.post('/nudge', testController.NudgeModel, testController.CustomNudgeModel, UserController.nudges)

router.post('/customNudge/add', testController.CustomNudgeModel, NudgeController.addCustomNudge)
router.post('/customNudge/show', testController.CustomNudgeModel, NudgeController.showCustomNudges)


// WellBeingNudges
router.post('/wellBeingNudges', testController.NudgeModel, testController.UserModel, testController.AdminNudgeModel, UserController.WellBeingNudges)

// Well being Audit
router.post('/wellBeingAudit', testController.QueryModel, QuestionsController.wellBeingAudit)
router.post('/detailedReview', testController.QueryModel, QuestionsController.detailedReview)
router.post('/detailedReviewOfCategory', testController.QueryModel, QuestionsController.detailedReviewOfCategory)

// Memos
router.post('/createMemo', testController.MemoModel, MemoController.createMemo)
router.post('/listMemos', testController.MemoModel, MemoController.listMemos)
router.post('/showMemo', testController.MemoModel, MemoController.showMemo)

// Wall
// Post
router.post('/createPost', multer.array('files'), testController.MediaModel, testController.PostModel, PostController.createPost)
router.post('/ListPosts', testController.MediaModel, testController.PostModel, testController.UserModel, PostController.ListPosts)
router.post('/fetchPost', testController.MediaModel, testController.PostModel, testController.UserModel, PostController.fetchPost)
router.post('/editPost', testController.PostModel, PostController.editPost)
router.post('/mediaDelete', testController.MediaModel, MediaController.deleteMedia)
router.post('/postDelete', testController.MediaModel,testController.PostModel, PostController.deletePost)


module.exports = router
