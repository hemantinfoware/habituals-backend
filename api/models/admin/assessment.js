const Sequelize = require('sequelize')
const connection = require('../../../services/sequelize.service').connection()

const options = {
    id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true
    },
    question: {
        type: Sequelize.STRING,
        required: true,
    },
    category: {
        type: Sequelize.STRING,
        required: true,
    },
}

const model = connection.define('Assessment', options)

exports.model = model
exports.options = options