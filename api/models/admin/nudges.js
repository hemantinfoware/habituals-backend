// This is the table where all the nudges, nudgeBoosters and their corresponding types are stored. These can be created, deleted or modified only by the admin.

const Sequelize = require('sequelize')
const connection = require('../../../services/sequelize.service').connection()

const options = {
    id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true
    },
    day: {
        type: Sequelize.INTEGER,
    },
    category: {
        type: Sequelize.STRING,
        required: true,
    },
    score: {
        type: Sequelize.INTEGER,
        required: true,
    },
    title: {
        type: Sequelize.STRING,
        required: true,
    },
    nudgeBooster: Sequelize.STRING(1200),

}

const model = connection.define('AdminNudges', options)

exports.model = model
exports.options = options