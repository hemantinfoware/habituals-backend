// Admin model

const Sequelize = require('sequelize')
const connection = require('../../../services/sequelize.service').connection()
const bcrypt = require('bcrypt')

const options = {
    id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true
    },
    email: {
        type: Sequelize.STRING,
        required: true,
        unique: true,
        allowNull: false,
    },
    password: {
        type: Sequelize.STRING,
        required: true,
        allowNull: false,
    },
    // mailingList: {
    //     type: Sequelize.BOOLEAN,
    //     default: true,
    // },
    // startDate: {
    //     type: Sequelize.DATE,
    //     required: true
    // },

    // age: Sequelize.INTEGER,
    // gender: Sequelize.STRING,
    // position: Sequelize.STRING,
}

const hooks = {
    hooks: {
        beforeCreate: async (record, options) => {
            record.dataValues.password = await bcrypt.hash(record.dataValues.password, 0).then((hash) => hash)
            console.log(record.dataValues.password)
        }
    }
}

const model = connection.define('Admin', options, hooks)

exports.options = options
exports.model = model
exports.hooks = hooks