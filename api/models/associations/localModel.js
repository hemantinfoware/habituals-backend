const UserSchema = require('../users')
const MemosSchema = require('../user/memos')
const NudgesSchema = require('../user/nudges')
const CustomNudgesSchema = require('../user/custom_nudges')
const QueriesSchema = require('../user/queries')
const AdminNudgeSchema = require('../admin/nudges')
const MediaSchema = require('../user/media')
const PostSchema = require('../user/post')

const UsersMemoSchema = require('../user/associations/UsersMemo.model')
const UsersQuerySchema = require('../user/associations/UserQuery.model')
const AdminUserNudgeSchema = require('../user/associations/AdminUserNudge')
const UserNudgeSchema = require('../user/associations/UserNudge')


const modelsController = require('../../middleware/site')

function oTo(A, B, C) {
    A.hasOne(B, {
        foreignKey: C
    });
    B.belongsTo(A, {
        foreignKey: C
    });
}

function oTm(A, B) {
    A.hasMany(B);
    B.belongsTo(A);
}

function oTmFk(A, B, C) {
    A.hasMany(B, { foreignKey: C });
    B.belongsTo(A, { foreignKey: C });
}

function oTmA(A, B, C) {
    A.hasMany(B, { as: C });
    B.belongsTo(A, { as: C });
}

function oTmAF(A, B, C) {
    A.hasMany(B, C);
    B.belongsTo(A, C);
}

function mTm(A, B, C) {
    A.belongsToMany(B, { through: C });
    B.belongsToMany(A, { through: C });
}

function mTmTr(A, B, C, D) {
    A.belongsToMany(B, { through: C, as: D });
    B.belongsToMany(A, { through: C, as: D });
}
function mTmTrUf(A, B, C, D) {
    A.belongsToMany(B, { through: { model: C, unique: false }, as: D });
    B.belongsToMany(A, { through: { model: C, unique: false }, as: D });
}

function mTmFk(A, B, C, D) {
    A.hasMany(B, { foreignKey: C, as: D });
    B.belongsTo(A, { foreignKey: C, as: D });
}

function mTmTFk(A, B, C, D) {
    A.belongsToMany(B, { through: C, foreignKey: D });
    B.belongsToMany(A, { through: C, foreignKey: D });
}

function mTmTFk2A(A, B, C, D, E, F) {
    A.belongsToMany(B, { through: C, foreignKey: D, as: F });
    B.belongsToMany(A, { through: C, foreignKey: E, as: F });
}

const createModels = async (id, connection) => {
    try {
        // Models
        const User = await connection.define('users_' + id, UserSchema.options, UserSchema.hooks)
        const Memo = await connection.define('memos_' + id, MemosSchema.options, { freezeTableName: true, })
        const Nudge = await connection.define('nudges_' + id, NudgesSchema.options, { freezeTableName: true, })
        const CustomNudge = await connection.define('customNudges_' + id, CustomNudgesSchema.options, { freezeTableName: true, })
        const Query = await connection.define('queries_' + id, QueriesSchema.options, { freezeTableName: true, })
        const Media = await connection.define('media_' + id, MediaSchema.options, { freezeTableName: true, })
        const Post = await connection.define('post_' + id, PostSchema.options, { freezeTableName: true, })

        // Association- Through Models

        // const UsersMemo = await connection.define('users_memos_' + id, UsersMemoSchema.options, { freezeTableName: true, })
        const UserNudge = await connection.define('u_nudge_' + id, UserNudgeSchema.options, { freezeTableName: true, modelName: 'singularName'})

        oTo(User, Query, "user")
        oTo(User, Memo, "user")
        // mTmTFk2A(User, Memo, UsersMemo, "user", "memo", "Memos")
        oTmFk(AdminNudgeSchema.model, Nudge, "adminNudge")
        oTmFk(User, CustomNudge, "user")
        mTmTFk2A(User, Nudge, UserNudge, "user", "nudge", "Nudges")
        oTmFk(Post, Media, "post")
        oTmFk(User, Post, "user")

        await connection.sync({alter: true, force: false})


        var allModels = {
            User,
            Memo,
            Nudge,
            CustomNudge,
            Query,
            Media,
            Post,
            // UsersMemo,
            // AdminUserNudge,
            UserNudge,
        }

        modelsController.addToModel(id, allModels)

        return allModels
    } catch (error) {
        console.log(error)
        return {
            status: false
        }
    }



}

const createAdminModel = async (id, connection) => {
    const AdminNudge = await connection.define('admin_nudge_' + id, AdminNudgeSchema.options)

    return {
        AdminNudge,
    }
}

module.exports = {
    createModels,
    createAdminModel,
}



// create database habituals_database