// These nudges are created by the user

const Sequelize = require('sequelize')
const connection = require('../../../services/sequelize.service').connection()

const options = {
    id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true
    },
    type: {
        type: Sequelize.STRING,
        // required: true,
    },
    status: {
        type: Sequelize.BOOLEAN,
        required: false,
        defaultValue: false
    },
    title: {
        type: Sequelize.STRING,
        required: true,
    },
    date: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW
    },
    nudgeBooster: Sequelize.TEXT,

}

const model = connection.define('CustomNudge', options)

exports.model = model
exports.options = options