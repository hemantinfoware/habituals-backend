const Sequelize = require('sequelize')

const connection = require('../../../services/sequelize.service').connection()

const options = {
    id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true
    },
    startDate: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW
    },

    starterQuery: {
        type: Sequelize.FLOAT,
        defaultValue: 2.5,
    },
    bodyQuery: {
        type: Sequelize.TEXT,
        get() { return getter(this, 'bodyQuery') },
        set(value) { return setter(this, 'bodyQuery', value) }
    },
    mindQuery: {
        type: Sequelize.TEXT,
        get() { return getter(this, 'mindQuery') },
        set(value) { return setter(this, 'mindQuery', value) }
    },
    personalDevelopmentQuery: {
        type: Sequelize.TEXT,
        get() { return getter(this, 'personalDevelopmentQuery') },
        set(value) { return setter(this, 'personalDevelopmentQuery', value) }
    },
    achievementQuery: {
        type: Sequelize.TEXT,
        get() { return getter(this, 'achievementQuery') },
        set(value) { return setter(this, 'achievementQuery', value) }
    },
    relationshipQuery: {
        type: Sequelize.TEXT,
        get() { return getter(this, 'relationshipQuery') },
        set(value) { return setter(this, 'relationshipQuery', value) }
    },
}

const model = connection.define('Queries', options)

exports.model = model
exports.options = options

function getter(thisVal, field) {
    let r
    try {
        r = JSON.parse(thisVal.getDataValue(field))
        return r
    } catch (error) {
        return {}
    }
}

function setter(thisVal, field, value) {
    return thisVal.setDataValue(field, JSON.stringify(value))
}