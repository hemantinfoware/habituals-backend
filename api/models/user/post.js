const Sequelize = require('sequelize')

const connection = require('../../../services/sequelize.service').connection()

const options = {
    id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true
    },
    content: {
        type: Sequelize.STRING,
        defaultValue: "...",
    }
}

const model = connection.define('Post', this.options)

exports.model = model
exports.options = options