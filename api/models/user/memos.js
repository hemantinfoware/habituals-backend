// Memos of an individual user

const Sequelize = require('sequelize')

const connection = require('../../../services/sequelize.service').connection()

const options = {
    id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true
    },
    title: {
        type: Sequelize.STRING,
        required: true,
    },
    date: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW
    },
    data: Sequelize.STRING(1200),
}

const model = connection.define('Memo', this.options)

exports.model = model
exports.options = options