const Sequelize = require('sequelize')

const connection = require('../../../services/sequelize.service').connection()

const options = {
    id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true
    },
    url: {
        type: Sequelize.STRING,
        required: true,
    },
    type: {
        type: Sequelize.STRING
    }
}

const model = connection.define('Media', this.options)

exports.model = model
exports.options = options