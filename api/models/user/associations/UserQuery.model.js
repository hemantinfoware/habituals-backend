// Query of an individual user

const Sequelize = require('sequelize')

const connection = require('../../../../services/sequelize.service').connection()

const options = {
    id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true
    },
}

const model = connection.define('UsersQuery', this.options)

exports.model = model
exports.options = options