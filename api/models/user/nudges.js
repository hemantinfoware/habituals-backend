// A huge nudge which handles all the nudges of all the users for each day

const Sequelize = require('sequelize')
const moment = require('moment')

const connection = require('../../../services/sequelize.service').connection()

const options = {
    id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true
    },
    // userId: Sequelize.STRING,
    // nudgeId: {
    //     type: Sequelize.UUID,
    //     defaultValue: Sequelize.UUIDV4
    // },
    status: {
        type: Sequelize.STRING,
        required: true,
    },
    date: {
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW
    },
}

const model = connection.define('Nudges', options)

exports.model = model
exports.options = options