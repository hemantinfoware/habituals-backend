const Sequelize = require('sequelize')
const connection = require('../../../services/sequelize.service').connection()


const SubDomains = connection.define('SubDomains', {
    id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true
    },

    subdomain: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true
    }
})

module.exports = SubDomains