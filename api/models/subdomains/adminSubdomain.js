const Sequelize = require('sequelize')
const connection = require('../../../services/sequelize.service').connection()


const AdminSubDomains = connection.define('AdminSubDomains', {
    id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true
    },

    subdomain: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true
    }
})

module.exports = AdminSubDomains