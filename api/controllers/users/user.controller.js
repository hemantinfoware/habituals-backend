const bcrypt = require('bcrypt')
const keys = require('../../../resources/keys')
const jwt = require('jsonwebtoken')
const secret = require('../../../resources/keys').tokenJWTKey
const QuestionsController = require('./questions.controller')

const AdminNudge = require('../../models/admin/nudges')

exports.register = async (req, res, next) => {
    if (!req.body.email)
        res.status(500).json({
            status: false,
            error: 'Email address cannot be empty',
        })
    else if (!req.body.password)
        res.status(500).json({
            status: false,
            error: 'Password cannot be empty',
        })

    else {
        try {
            req.UserModel.create({
                age: req.body.age,
                gender: req.body.gender,
                password: req.body.password,
                position: req.body.position,
                mailingList: req.body.mailingList,
                email: req.body.email,
                startDate: req.body.startDate ? req.body.startDate : Date()
            })
                .then((data) => {
                    res.send(data)
                })
                .catch((err) => {
                    console.log(err)
                    res.send(err)
                })
        } catch (err) {
            res.status(500).json({
                status: false,
                error: err.message,
            })
        }
    }
}


exports.login = (req, res, next) => {
    if (!req.body.email)
        res.status(500).json({
            status: false,
            error: 'Email address cannot be empty',
        })
    else if (!req.body.password)
        res.status(500).json({
            status: false,
            error: 'Password cannot be empty',
        })
    else {
        req.UserModel.findOne({
            where: {
                email: req.body.email,
            }
        }).then((user) => {
            if (user === null) {
                res.status(404).json({
                    status: false,
                    error: 'Email Address does not exist',
                })
            } else {
                bcrypt.compare(req.body.password, user.password)
                    .then((result) => {
                        const token = result ? jwt.sign(user.id, secret) : "Wrong password"
                        res.send({
                            status: result,
                            token,
                            user
                        })
                    })
                    .catch((err) => {
                        res.send({
                            status: false,
                        })
                    })
            }
        })
            .catch(err => {
                res.send({
                    error: true,
                    message: err.message
                })
            })
    }
}

exports.startSession = async (req, res) => {
    const _b = req.body
    const userId = _b.userId
    const startDate = new Date(parseInt(_b.startDate))
    Date.prototype.addDays = function (days) {
        var date = new Date(startDate)
        date.setDate(date.getDate() + days)
        return date
    }

    let date = new Date()

    let values = []
    let UserNudgeValues = []

    await req.QueryModel.findOne({
        where: {
            user: userId,
        }
    })
        .then(queries => {

            if (queries == null) throw new Error("Queries not seeded yet")

            const response = QuestionsController.calculateScoreFromQuestion(queries.toJSON())
            console.log(response)

            AdminNudge.model
                .findAll({
                    order: [
                        ['day', 'ASC']
                    ]
                })
                .then((data) => {
                    let categories = []

                    function addDataToValues(id, x) {
                        values.push({
                            status: "not completed",
                            date: date.addDays(parseInt(x)),
                            adminNudge: id
                        })
                    }

                    for (var x in data) {
                        if (!categories.includes(x.category)) categories.push(x.category)


                        if (data[x].category === 'mind') {
                            console.log(Math.ceil(response.mindQuery))
                            if (Math.ceil(response.mindQuery) === data[x].score || Math.floor(response.mindQuery) === data[x].score) {
                                addDataToValues(data[x].id, data[x].day)
                            }
                        } else if (data[x].category === 'body') {
                            if (Math.ceil(response.bodyQuery) === data[x].score || Math.floor(response.bodyQuery) === data[x].score) {
                                addDataToValues(data[x].id, data[x].day)
                            }
                        } else if (data[x].category === 'personalDevelopment') {
                            if (Math.ceil(response.personalDevelopmentQuery) === data[x].score || Math.floor(response.personalDevelopmentQuery) === data[x].score) {
                                addDataToValues(data[x].id, data[x].day)
                            }
                        } else if (data[x].category === 'relationship') {
                            if (Math.ceil(response.relationshipQuery) === data[x].score || Math.floor(response.relationshipQuery) === data[x].score) {
                                addDataToValues(data[x].id, data[x].day)
                            }
                        } else {
                            if (Math.ceil(response.achievementQuery) === data[x].score || Math.floor(response.achievementQuery) === data[x].score) {
                                addDataToValues(data[x].id, data[x].day)
                            }
                        }

                    }

                    req.NudgeModel.bulkCreate(values, {
                        returning: true
                    })
                        .then((data) => {

                            for (var x of data) {
                                UserNudgeValues.push({
                                    user: userId,

                                    nudge: x.id
                                })
                            }

                            req.UserNudgeModel.bulkCreate(UserNudgeValues, {
                                returning: true
                            })
                                .then((data) => {

                                    res.send(data)
                                })
                                .catch((err) => {
                                    console.log(err)
                                    res.send(err)
                                })

                        })
                        .catch((err) => {
                            console.log(err)
                            res.send(err)
                        })

                })
                .catch(err => {
                    console.log(err)
                    res.send(err)
                })
        })
        .catch(err => {
            res.send({
                error: err.message
            })
        })



}

exports.WellBeingNudges = async (req, res) => {
    req.UserModel.findAll({
        where: {
            id: req.body.userId
        },
        include: [{
            model: req.NudgeModel,
            include: [{
                model: req.AdminNudgeModel,
                order: [
                    ['day', 'ASC']

                ]
            }],
            as: "Nudges",
        }],


        // to fetch in single array, will provide chances of sorting day wise by FE
        nest: false,
        raw: true,
        // order: [
        //     ['nudges_xaba3s.AdminNudge.day', "ASC"]
        // ]

        // can't read type of null. Will have to think a better way. May be because of dummy data.
    })
        .then(data => {
            res.send(data)
        })
        .catch(err => {
            console.log(err)
            res.send(err)
        })

}


exports.profile = (req, res, next) => {
    // const userId = req.auth.userId
    // const userId = realtime.auth.userId
    const userId = req.body.userId
    // req.body.userId only for dev purpose

    try {
        req.UserModel.findOne({
            where: {
                id: userId,
            },
            include: [{
                model: req.QueryModel,
            }]
        })
            .then((data) => {
                console.log(data)
                res.send(data)
            })
            .catch(err => {
                console.log(err)
                res.send(err)
            })

    } catch (error) {
        console.log(error)
        res.send(error)
    }

}
