exports.createMemo = async (req, res) => {
    const _b = req.body
    const userId = req.body.userId
    req.MemoModel.create({
        title: _b.title,
        data: _b.data,
        user: userId
    })
        .then((data) => {
            res.send(data)
        })
        .catch((err) => {
            res.send(err)
        })
}

exports.listMemos = async (req, res) => {
    const _b = req.body
    const userId = _b.userId
    req.MemoModel.findAll({
        where: {
            user: userId
        },
        order: [
            ['createdAt', 'DESC']
        ]
    })
        .then((data) => {
            res.send(data)
        })
        .catch(err => {
            console.log(err)
            res.send(err)
        })
}

exports.showMemo = async (req, res) => {
    const _b = req.body
    const userId = _b.userId

    req.MemoModel.findOne({
        where: {
            user: userId,
            id: _b.memoId
        },
    })
        .then((data) => {
            if (data != null)
                res.send(data)
            else throw new Error("Not found")
        })
        .catch(err => {
            res.send({
                error: err.message,
                status: false
            })
        })
}
