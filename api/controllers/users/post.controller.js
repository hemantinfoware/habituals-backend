const mediaController = require('./media.controller')

exports.createPost = (req, res) => {
    const _b = req.body
    const userId = _b.userId

    req.PostModel.create({
        content: _b.content,
        user: userId
    })
        .then(data => {
            console.log(data)
            req.body.postId = data.id
            if (req.dir == null) {
                return res.send(data)
            }
            mediaController.uploadMedia(req, res)
        })
        .catch(err => {
            console.log(err)
            res.send(err)
        })
}

exports.ListPosts = async (req, res) => {
    const _b = req.body

    req.PostModel.findAll({
        include: [{
            model: req.MediaModel,

        }, {
            model: req.UserModel
        }],

        order: [
            ['updatedAt', 'DESC']
        ],

        limit: parseInt(_b.limit),
        offset: parseInt(_b.offset)

    })
        .then(data => {
            res.send(data)
        })
        .catch(err => {
            res.send(err)
        })
}

exports.fetchPost = (req, res) => {
    const _b = req.body

    req.PostModel.findOne({
        where: {
            id: _b.postId
        },
        include: [{
            model: req.MediaModel,
        }, {
            model: req.UserModel,
        }],
    })
    .then((data) => {
        res.send(data)
    })
    .catch(err => {
        res.send(err)
    })
}

exports.editPost = (req, res) => {
    const _b = req.body

    req.PostModel.findOne({
        where: {
            id: _b.postId
        }
    })
    .then(data => {
        if (data == null) throw new Error('No post found')

        data.update({
            content: _b.content
        })
        .then((val) => {
            console.log(val)
            res.send(val)
        })
        .catch(err => {
            console.log(err)
            res.send(err)
        })
    })
    .catch(err => {
        console.log(err)
        res.send({
            error: err.message
        })
    })
}

// exports.deletePost = (req, res) => {
//     const _b = req.body
    
//     req.PostModel.destroy({
//         where: {
//             id: _b.postId,
//         },

//         // Apply onDelete= 'CASCADE'
//     })
//     .then(data => {
//         res.send({
//             data
//         })
//     })
//     .catch(err => {
//         res.send(err)
//     })
// }

exports.deletePost = (req, res) => {
    const _b = req.body

    req.MediaModel.destroy({
        where: {
            post: _b.postId
        }
    })
    .then(_ => {
        req.PostModel.destroy({
            where: {
                id: _b.postId
            }
        })
        .then(count => {
            res.send({
                count
            })
        })
        .catch(e => {
            res.send(e)
        })
    })
}