const Assessments = require('../../models/admin/assessment')

function calculateScoreFromQuestion(req) {
    function auditScore(jsonFile) {
        var sum = 0

        for (let i in jsonFile) {
            sum += jsonFile[i]
        }
        return sum / 5
    }
    // const userId = req.auth.userId
    const userId = req.userId

    const startDate = req.startDate
    const starter = req.starterQuery
    const body = req.bodyQuery
    const mind = req.mindQuery
    const achievement = req.achievementQuery
    const relationship = req.relationshipQuery
    const personalDevelopment = req.personalDevelopmentQuery

    return {
        userId: userId,
        startDate: startDate,
        starterQuery: starter,
        bodyQuery: auditScore(body),
        mindQuery: auditScore(mind),
        achievementQuery: auditScore(achievement),
        relationshipQuery: auditScore(relationship),
        personalDevelopmentQuery: auditScore(personalDevelopment),
    }

}

exports.seedQuestions = (req, res, next) => {
    const response = req.body

    req.QueryModel.findOrCreate({
        where: {
            user: req.body.userId
        },
        defaults: {
            startDate: response.startDate,
            starterQuery: response.starterQuery,
            bodyQuery: response.bodyQuery,
            mindQuery: response.mindQuery,
            achievementQuery: response.achievementQuery,
            relationshipQuery: response.relationshipQuery,
            personalDevelopmentQuery: response.personalDevelopmentQuery,
        }
    })
        .then((data) => {
            const dataAt0 = data[0].toJSON()
            res.send({
                ...dataAt0,
                newField: data[1]
            })
        })
        .catch(err => {
            console.log(err)
            res.send(err)
        })
}

exports.updateQuestions = async (req, res) => {
    const response = req.body

    let props = {}

    req.body.bodyQuery !== undefined ? props.bodyQuery = response.bodyQuery : null
    req.body.mindQuery !== undefined ? props.mindQuery = response.mindQuery : null
    req.body.achievementQuery !== undefined ? props.achievementQuery = response.achievementQuery : null
    req.body.relationshipQuery !== undefined ? props.relationshipQuery = response.relationshipQuery : null
    req.body.personalDevelopmentQuery !== undefined ? props.personalDevelopmentQuery = response.personalDevelopmentQuery : null

    req.QueryModel.update(props, {
        where: {
            user: req.body.userId,
        },
        limit: 1,
    })
        .then((data) => {
            console.log(data)
            res.send(data)
        })
        .catch(err => {
            console.log(err)
            res.send(err)
        })
}

exports.wellBeingAudit = async (req, res) => {
    const _b = req.body

    req.QueryModel.findOne({
        where: {
            user: _b.userId,
        }
    })
        .then((data) => {
            const response = calculateScoreFromQuestion(data.toJSON())

            res.send(response)
        })
        .catch((err) => {
            res.send(err.message)
        })
}

exports.detailedReview = async (req, res) => {
    const _b = req.body

    req.QueryModel.findOne({
        where: {
            user: _b.userId,
        }
    })
        .then((data) => {
            const audit = calculateScoreFromQuestion(data.toJSON())
            delete audit.userId
            delete audit.startDate
            res.send({
                ...data.toJSON(),
                audit
            })
        })
        .catch(err => res.send(err))
}

exports.detailedReviewOfCategory = (req, res) => {
    const _b = req.body

    req.QueryModel.findOne({
        where: {
            user: _b.userId,
        },
        attributes: [
            _b.category + 'Query'
        ]
    })
    .then(data => {
        let thisQuery = data.toJSON()[_b.category + 'Query']
        Assessments.model.findAll({
            where: {
                category: _b.category
            }
        })
            .then(asses => {
                let scoreCard = []
                for (let x of asses) {
                    let obj = x.toJSON()
                    let scoreDef = {}

                    scoreDef.question = obj.question
                    scoreDef.category = obj.category
                    scoreDef.score = thisQuery[obj.id]

                    scoreCard.push(scoreDef)
                }
                res.send(scoreCard)
            })
            .catch(err => res.send({
                error: err.message
            }))
    })
    .catch(err => res.send(err))
}








exports.calculateScoreFromQuestion = calculateScoreFromQuestion