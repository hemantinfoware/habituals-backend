exports.addCustomNudge = (req, res) => {
    const _b = req.body

    // const userId = req.auth || req.authUser.id || req.body.userId
    const userId = _b.userId
    _b.userId = userId

    req.CustomNudgeModel.create({
        title: _b.title,
        date: Date(parseInt(_b.date)),
        nudgeBooster: _b.nudgeBooster,
        user: _b.userId,
    })
    .then((data) => {
        // console.log(data)
        res.send(data)
    })
    .catch(e => {
        // console.log(e)
        res.send(e)

    })
}

exports.showCustomNudges = (req, res) => {
    const _b = req.body

    const userId = _b.userId

    req.CustomNudgeModel.findAll({
        where: {
            user: userId
        }
    })
    .then((data) => {
        console.log(data)
        res.send(data)
    })
    .catch(e => {
        console.log(e)
        res.send(e)
    })
}