const path = require('path')

exports.uploadMedia = (req, res) => {
    const _b = req.body
    const _d = req.dir

    let medias = []
    
    for (let x of _d) {
        let oneMedia = {}
        oneMedia.post = _b.postId
        oneMedia.type = x.type
        oneMedia.url = path.join(__dirname, "../../../uploads/" + x.fullName)

        medias.push(oneMedia)
    }


    req.MediaModel.bulkCreate(medias)
        .then(data => {
            let response = {}
            response.body = req.body
            response.media = data
            res.send(response)
        })
        .catch(err => {
            res.send(err)
        })
}

exports.deleteMedia = async (req, res) => {
    const _b = req.body

    // const flag = await mediaDeleteFunction(_b.postId, req.MediaModel)
    req.MediaModel.destroy({
        where: {
            id: _b.mediaId,
        }
    }).then((data) => {
        console.log(data)
        res.send({
            count: data,
        })
    })
    .catch(err => {
        console.log(err)
        res.send(err)
    })

}
