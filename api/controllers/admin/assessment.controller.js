const Assessment = require('../../models/admin/assessment').model

exports.seedAssessment = async (req, res) => {
    const _b = req.body

    let data = []
    const queries = req.body.queries

    for (var property in queries) {
        const value = queries[property]

        for (var x of value) {
            var obj = {}

            obj.question = x
            obj.category = property

            data.push(obj)
        }
    }

    Assessment.bulkCreate(data)
        .then(val => {
            res.send(val)
        })
        .catch(err => {
            res.send(err)
        })

}

exports.fetchQuestions = (req, res) => {
    Assessment.findAll({
        order: [
            ['createdAt', 'DESC']
        ]
    })
        .then(data => {
            let categories = []
            let response = {}
            for (let x of data) {
                let json = x.toJSON()
                if (!categories.includes(json.category)) {
                    categories.push(json.category)
                    response[json.category] = []
                }
                response[json.category].push(json)
            }
            res.send(response)
        })
        .catch(err => {
            res.send(err)
        })
}