const Admin = require('../../models/admin/admin').model
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')
const secret = require('../../../resources/keys').tokenJWTKey
const connection = require('../../../services/sequelize.service').connection()
const { createModels } = require('../../models/associations/localModel')
const SubDomain = require('../../models/subdomains/subdomains')
const AdminNudge = require('../../models/admin/nudges').model

exports.addAdmin = async (req, res) => {
    const _b = req.body

    Admin.create(_b)
        .then((data) => res.send(data))
        .catch(e => res.send(e))
}

exports.loginAdmin = async (req, res) => {
    const _b = req.body

    Admin.findOne({
        where: {
            email: _b.email,
        }
    })
        .then((data) => {
            if (data === null)
                throw new Error("No data found")
            const details = data.dataValues

            bcrypt.compare(_b.password, details.password)
                .then((result) => {

                    const token = result ? jwt.sign(details.id, secret) : "Wrong password"
                    res.send({
                        status: result,
                        token
                    })
                })
                .catch(err => {
                    res.send({
                        status: false
                    })
                })
        })
        .catch((e) => {
            res.send(e.message)
        })
}

exports.addSubDomain = async (req, res) => {
    const _b = req.body
    SubDomain.create({
        subdomain: _b.subdomain
    })
        .then(async (data) => {
            console.log(data.id)
            const models = await createModels(data.subdomain, connection)
                .then(async (model) => {
                    await connection.sync()
                    res.send({
                        status: true,
                    })
                })
                .catch((err) => {
                    res.send(err)

                })
        })
        .catch((e) => {
            res.send(e)
        })

}

exports.AddAdminNudge = async (req, res) => {
    const x = req.body
    AdminNudge.create({
        category: x.category,
        title: x.title,
        nudgeBooster: x.nudgeBooster,
        day: x.day,
        score: x.score,
    })
        .then((data) => {
            res.send(data)
        })
        .catch(e => {
            res.send(e)
        })
}

exports.AddAdminNudgeBulk = async (req, res) => {
    const _b = req.body
    let values = []

    for (var x of _b) {
        values.push({
            category: x.category,
            title: x.title,
            nudgeBooster: x.nudgeBooster,
            day: x.day,
            score: x.score,
        })
    }

    AdminNudge.bulkCreate(values, {
        returning: true
    })
        .then((data) => {
            // console.log(data)
            res.send(data)
        })
        .catch((err) => {
            console.log(err)
            res.send(err)
        })
}

exports.listAllSubdomains = async (req, res) => {
    SubDomain.findAll({})
        .then((data) => {
            res.send(data)
        })
        .catch((err) => {
            console.log(err)
            res.send(err)
        })
}

exports.listAllAdmins = async (req, res) => {
    Admin.findAll({})
        .then((data) => {
            res.send(data)
        })
        .catch((err) => {
            console.log(err)
            res.send(err)
        })
}