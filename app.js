const http = require('http')
const morgan = require('morgan')
const express = require('express')
const path = require('path')
// const bodyParser = require('body-parser')
// const formData = require('form-data')
// const Sequelize = require('sequelize')

// Connecting to MySQL server

const sequelize = require('./services/sequelize.service')
sequelize.connect()


const keys = require('./resources/keys')
const userRoutes = require('./api/routes/users')
const adminRoutes = require('./api/routes/admin')


const app = express()
var db = {}

app.use('/uploads', express.static('uploads'));
app.use(express.static(path.join(__dirname, 'public')));

// Handling CORS
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With, Content-Type, Accept, Authorization"
    );
    if (req.method === "OPTIONS") {
        res.header("Access-Control-Allow-Methods", "PUT, POST, PATCH, DELETE, GET");
        return res.status(200).json({
            message: "request granted",
        });
    }
    next();
});



require('./api/models/associations/association')
require('./api/models/subdomains/subdomains')


// Middlewares
app.use(morgan("dev"));
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
// app.use(bodyParser.json({ extended: true, limit: '50mb' }));
// app.use(bodyParser.urlencoded({ extended: true, limit: '50mb' }));

// Routes
app.use('/user', userRoutes)
app.use('/admin', adminRoutes)

// Throw 404 error if the request does not match an existing route
app.use((req, res, next) => {
    const error = new Error()
    error.status = 404
    error.message = '404 route not found'
    next(error)
})

//  Return the error thrown by any part of the project.
app.use((err, req, res, next) => {
    res.status(err.status || 404).json({
        status: false,
        error: err.message,
    })
});

sequelize.connection()
    .authenticate()
    .then(() => {
        return sequelize.connection().sync({ alter: true, force: false });
    })
    .then(() => {
        // Running the server on the given port
        const port = process.env.PORT || keys.port
        const server = http.createServer(app)
        server.listen(port, () => {
            console.log("\x1b[33m", `\nServer running on port: ${port}`)
            console.log("\x1b[37m", '')
        })
    })

// // Running the server on the given port
// const port = process.env.PORT || keys.port
// const server = http.createServer(app)
// server.listen(port, () => {
//     console.log("\x1b[33m", `\nServer running on port: ${port}`)
//     console.log("\x1b[37m", '')
// })