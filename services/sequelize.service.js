const dbConfig = require('../resources/keys').db;
const SequelizeService = require("sequelize");

let sequelize;

module.exports = {
    connect: () => {
        return sequelize = new SequelizeService(dbConfig.databaseName, dbConfig.userName, dbConfig.userPassword, {
            host: dbConfig.baseUrl,
            dialect: dbConfig.dialect,
            // timezone: "+5:30",
            // pool: {
            //     max: dbConfig.pool.max,
            //     min: dbConfig.pool.min,
            //     acquire: dbConfig.pool.acquire,
            //     idle: dbConfig.pool.idle
            // },
            logging: console.log
        });
    },
    auth : () => {
        sequelize
            .authenticate()
            .then(() => {
                console.log('Connection has been established successfully.');
            })
            .catch(err => {
                console.error('Unable to connect to the database:', err);
            });
        return sequelize;
    },
    connection : () => {
        return sequelize;
    }
};
